-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 07, 2021 at 01:35 PM
-- Server version: 5.7.31
-- PHP Version: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `exam_management_system_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `exams`
--

DROP TABLE IF EXISTS `exams`;
CREATE TABLE IF NOT EXISTS `exams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_id` varchar(100) NOT NULL,
  `exam_title` varchar(100) NOT NULL,
  `exam_type` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exams`
--

INSERT INTO `exams` (`id`, `exam_id`, `exam_title`, `exam_type`) VALUES
(1, '120', 'تصميم وبرمجة الإنترنت', '1'),
(2, '130', 'قواعد بينات', '2'),
(42, '500', 'تصميم وبرمجة الإنترنت سنة ثانية', '2'),
(44, '222', 'تجربة', '1');

-- --------------------------------------------------------

--
-- Table structure for table `exam_type_tbl`
--

DROP TABLE IF EXISTS `exam_type_tbl`;
CREATE TABLE IF NOT EXISTS `exam_type_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=225 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exam_type_tbl`
--

INSERT INTO `exam_type_tbl` (`id`, `name`) VALUES
(1, 'Multiple Choice	'),
(2, 'True & False	');

-- --------------------------------------------------------

--
-- Table structure for table `mcq_tbl`
--

DROP TABLE IF EXISTS `mcq_tbl`;
CREATE TABLE IF NOT EXISTS `mcq_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qtitle` varchar(10000) NOT NULL,
  `Choice_1` varchar(1000) NOT NULL,
  `Choice_2` varchar(1000) NOT NULL,
  `Choice_3` varchar(1000) NOT NULL,
  `Choice_4` varchar(1000) NOT NULL,
  `answer` varchar(1000) NOT NULL,
  `exam_id` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mcq_tbl`
--

INSERT INTO `mcq_tbl` (`id`, `qtitle`, `Choice_1`, `Choice_2`, `Choice_3`, `Choice_4`, `answer`, `exam_id`) VALUES
(2, 'أي عبارى مما يلي تصف التجارة الإلكترونية ؟', 'بيع البضائع', 'ممارسة الأعمال التجارية الكترونياً', 'ممارسة أنشطة الأعمال', 'بيع الخدمات', 'ممارسة الأعمال التجارية الكترونياً', '120'),
(3, 'ما هي منصات العمل الحر ؟', 'عبارة عن مواقع على الأنترنت متخصصة في العمل الحر . تعمل هذة المنصات كوسيط بين مقدمي الخدمات والذين يحتاجون لهذه الخدمات', 'هي منصات تتبع وحدة التحكم (Dashboard) أحترافية لكل من مقدمي الخدمات وطالبيها', 'هي منصات ذات نظام محدد لديها شروط وقوانين لسير العمل وتنظيم التعامل مع الأطراف المتعاقدة', 'كل الإجابات صحيحة', 'كل الإجابات صحيحة', '120\r\n'),
(11, 'ما أسمك', 'أحمد محمد أحمد فرحات', 'غريب علي الدين', 'علاء الدين', 'علي سميح مسمح', 'أحمد محمد أحمد فرحات', '120'),
(12, 'ما اسمك', '3333', 'أحمد فرحات', 'علي سميح', 'ابراهيم ابو شنب', 'أحمد فرحات', '120'),
(13, 'ما أسمك', 'أحمد محمد أحمد فرحات', 'علي', 'سامر', 'تامر علينا', 'أحمد محمد أحمد فرحات', '222'),
(5, 'ما المقصود ب ال web Storage في ال Html ؟', 'اي تخزين البينات محلياً داخل قواعد البينات', 'اي تخزين البينات محلياً داخل متصفح المستخدم', 'اي تخزين البينات محلياً داخل مصفوفة', 'اي تخزين البينات', 'اي تخزين البينات محلياً داخل متصفح المستخدم', '120'),
(7, 'ما هي منصات العمل الحر ؟', 'عبارة عن مواقع على الأنترنت متخصصة في العمل الحر . تعمل هذة المنصات كوسيط بين مقدمي الخدمات والذين يحتاجون لهذه الخدمات', 'هي منصات تتبع وحدة التحكم (Dashboard) أحترافية لكل من مقدمي الخدمات وطالبيها', 'هي منصات ذات نظام محدد لديها شروط وقوانين لسير العمل وتنظيم التعامل مع الأطراف المتعاقدة', 'كل الإجابات صحيحة', 'كل الإجابات صحيحة', '120'),
(8, 'تعبتر قواعد البينات الشبكية من اقدم انواع الشبكات ؟', 'اي تخزين البينات محلياً داخل قواعد البينات', 'ممارسة الأعمال التجارية الكترونياً', 'اي تخزين البينات محلياً داخل مصفوفة', 'اي تخزين البينات', 'اي تخزين البينات محلياً داخل قواعد البينات', '120'),
(10, 'ما هي منصات العمل الحر ؟', '3333', '4444', '55555', '7777', '3333', '120');

-- --------------------------------------------------------

--
-- Table structure for table `true_false_tbl`
--

DROP TABLE IF EXISTS `true_false_tbl`;
CREATE TABLE IF NOT EXISTS `true_false_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qtitle` varchar(1000) NOT NULL,
  `answer` varchar(100) NOT NULL,
  `exam_id` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `true_false_tbl`
--

INSERT INTO `true_false_tbl` (`id`, `qtitle`, `answer`, `exam_id`) VALUES
(1, 'تعبتر قواعد البينات الشبكية من اقدم انواع الشبكات ؟', 'True', '130'),
(2, 'قواعد البينات هي مجموعة من البرامج التي يمكن استحدامها في انشاء', 'False', '130'),
(3, 'البيانات التي تصف البينات المخزنه وصفاً دقيقاً ويطلق عليها البيا', 'True', '130'),
(9, 'هل انت من تخصص انترنت سنة 2', 'True', '500'),
(10, 'هل تدرس لغة php', 'True', '500');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
