<?php
include ("config/input_validation.php");
include_once ("config/Database.php");
?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <style type="text/css>">

        .ahmed{
            margin-right: 20px;
        }
    </style>
    <title> إضافة اسئلة امتحان  </title>
</head>



<?php
// Ahmed Farhat Finel exame


 $questionErr =  $genderErr = "";
 $question =  $gender =$exam_id = "";


if ($_SERVER["REQUEST_METHOD"] == "GET") {
    $id = $_GET['id'];
    $DBObject = new Database("localhost", "root", "", "exam_management_system_db");
    $DBObject->query ("SET NAMES utf8");
    $DBObject->query ("SET CHARACTER SET utf8");
    $sql = "Select * from exams where id='". $id ."'";
    $val = $DBObject->query($sql);
    $row = $val->fetch();
    $exam_id=$row['exam_id'];
}


if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $val_object = new validation();
    $id=$_POST['id'];
    $exam_id = $_POST['exam_id'];
    $question = $val_object->test_input($_POST['question']);
    if(isset($_POST['gender'])) $gender = $val_object->test_input($_POST['gender']);


    /*
        $result=$val_object->input_validation($question,"/^[\p{L} ]+$/u","الرجاء ادخال سؤال الامتحان","لا يمكنك وضع ايه رموز في  سؤال الامتحان ");
        $numErr=$result[0];
        $Check['num']=$result[1];
    */


    $result=$val_object->selection_validation($question,"الرجاء ادخال سؤال الامتحان");
    $questionErr=$result[0];
    $Check['num']=$result[1];


    $result=$val_object->selection_validation($gender,"الرجاء اختيار الاجابة الصحيحة ");
    $genderErr=$result[0];
    $Check['gender']=$result[1];

    foreach($Check as $value)
    {
        if($value==false)
        {
            $flag=false;
            break;
        }
        $flag=true;
    }

    if($flag==true){
        // Ahmed Farhat Final exam
        $DBObject = new Database("localhost", "root", "", "exam_management_system_db");
        $DBObject->query ("SET NAMES utf8");
        $DBObject->query ("SET CHARACTER SET utf8");
        $sql = "INSERT INTO true_false_tbl  (qtitle, answer, exam_id )
                              VALUES ('$question', '$gender', '$exam_id')";


        if($DBObject->execute($sql)) {
            header("location:index.php");
            exit(0);
        }

    }

}
?>


<br>

<body dir="rtl">
&nbsp; <h1  style="margin-right: 20px">إضافة اسئلة امتحان </h1>
<hr>

<p style="margin-right: 35px">يرجا ملء النموذج لاضافة اسئلة جديدة على الامتحان</p>
<div class="col-6 " style="margin-right: 50px">
    <form class="ahmed" accept-charset="utf-8" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        <div class="mb-3">
            <label for="exampleFormControlInput1" class="form-label">رقم الامتحان </label>
            <input type="text" name="id" class="form-control" style="pointer-events: none;background-color:#E9ECEF;" value="<?php echo $id?>">
            <input type="hidden" name="exam_id" class="form-control" style="pointer-events: none;background-color:#E9ECEF;" value="<?php echo $exam_id ?>">
        </div>

        <div class="mb-3">
            <label for="exampleFormControlInput1" class="form-label"> السؤال  </label>
            <input type="text" name="question" class="form-control" id="exampleFormControlInput1">
            <span ><?php echo $questionErr;?></span>

        </div>

        <label for="exampleFormControlInput1" class="form-label" for="gender"> </label>
        <select id="gender" name="gender" class="form-select" aria-label="Default select example">
            <option selected></option>
            <option name="gender" value="False" <?php if ($gender == "False") echo 'selected="selected"';?>>False</option>
            <option name="gender" value="True" <?php if ($gender == 'True') echo 'selected="selected"';?>> True </option>

        </select>
        <span ><?php echo $genderErr;?></span>



        <br>
        <br>
        <div class="col-md-6 mb-3">
            <input type="submit" name="submit" value="ادخال "  class="btn btn-dark">
            <a href="index.php" type="button" class="btn btn-outline-dark">الغاء</a>
        </div>
    </form>


</div>



<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>


</body>
</html>
