<?php
include ("config/input_validation.php");
include_once ("config/Database.php");
?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
<style type="text/css>">

    .ahmed{
        margin-right: 20px;
    }
</style>
    <title>إضافة امتحان </title>
</head>



<?php
// Ahmed Farhat Finel exame

$numErr = $name_examErr = $genderErr =  "";
$num = $name_exam =  $gender =  "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $val_object = new validation();

    $name_exam = $val_object->test_input($_POST['name_exam']);
    $num = $val_object->test_input($_POST['num']);
    if(isset($_POST['gender'])) $gender = $val_object->test_input($_POST['gender']);

    $result=$val_object->input_validation($name_exam,"/^[\p{L} ]+$/u","الرجاء ادخال اسم الاختبار","يسمح بكتابة اسم الاختبار بالغة العربية فقط ويمنع ادخال اية رموز");
    $name_examErr=$result[0];
    $Check['name_exam']=$result[1];

    $result=$val_object->input_validation($num,"/^[0-9]+$/","الرجاء ادخال رقم الامتحان","يسمح فقط بلارقام");
    $numErr=$result[0];
    $Check['num']=$result[1];


    $result=$val_object->selection_validation($gender,"الرجاء حدد نوع السؤال");
    $genderErr=$result[0];
    $Check['gender']=$result[1];


    if ($gender == "اختيار من متعدد" ){
        $gender = 1;
    }
    elseif ($gender=="صح أو خطاء"){
        $gender = 2;
    }


    foreach($Check as $value)
    {
        if($value==false)
        {
            $flag=false;
            break;
        }
        $flag=true;
    }

    if($flag==true){
        // Ahmed Farhat Final exam
        $DBObject = new Database("localhost", "root", "", "exam_management_system_db");
        $DBObject->query ("SET NAMES utf8");
        $DBObject->query ("SET CHARACTER SET utf8");
        $sql = "INSERT INTO exams  (exam_id, exam_title, exam_type )
                              VALUES ('$num', '$name_exam', '$gender')";


        if($DBObject->execute($sql)) {
            header("location:index.php");
            exit(0);
        }

    }

}
?>

<br>

<body dir="rtl">
&nbsp; <h1  style="margin-right: 20px">إضافة امتحان </h1>
<hr>

<p style="margin-right: 35px">يرجى ملئ هذا النموذج لاضافة امتحان جديد </p>
<div class="col-6 " style="margin-right: 50px">
    <form class="ahmed" accept-charset="utf-8" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?> ">

        <div class="mb-3">
        <label for="exampleFormControlInput1" class="form-label">رقم الامتحان </label>
        <input type="text" name="num" class="form-control" id="exampleFormControlInput1" placeholder="ادخل رقم الامتحان" value="<?php echo $num?>">
        <span><?php echo $numErr;?></span>

</div>

<div class="mb-3">
    <label for="exampleFormControlInput1" class="form-label"> اسم الامتحان  </label>
    <input type="text" name="name_exam" class="form-control" id="exampleFormControlInput1" placeholder="ادخل اسم الامتحان "value="<?php echo $name_exam?>">
    <span ><?php echo $name_examErr;?></span>

</div>

        <label for="exampleFormControlInput1" class="form-label" for="gender"> نوع الامتحان </label>
        <select id="gender" name="gender" class="form-select" aria-label="Default select example">
            <option selected></option>
            <option name="gender" value="اختيار من متعدد" <?php if ($gender == "اختيار من متعدد") echo 'selected="selected"';?>> Multiple Choice</option>
            <option name="gender" value="صح أو خطاء" <?php if ($gender == 'صح أو خطاء') echo 'selected="selected"';?>> True & False</option>

        </select>
        <span ><?php echo $genderErr;?></span>

        <br>
        <br>
        <div class="col-md-6 mb-3">
            <input type="submit" name="submit" value="ادخال "  class="btn btn-dark">
            <a href="index.php" type="button" class="btn btn-outline-dark">الغاء</a>
        </div>
    </form>


</div>



<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>


</body>
</html>
