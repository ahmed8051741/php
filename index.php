<?php
include ("config/input_validation.php");
include_once ("config/Database.php");
?>
<!DOCTYPE html>
<html lang="ar">

<head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="css/all.min.css">


    <style type="text/css>">

        .ahmed{
            margin-right: 20px;
        }

    </style>
    <title>  نظام ادارة الامتحانات</title>
</head>


<body dir="rtl"class="col-md-12 mb-12">
<div class="col-md-6 mb-3" style="float: right">
    <h1>
        &nbsp; نظام ادارة الامتحانات
    </h1>
</div>
<div class="col-md-6 mb-3" style="float:left ; margin-top:20px">

<a href="create.php"><button type="button" class="btn btn-dark" style="float: left">إضافة امتحان جديد</button></a>
</div>

<br>
<br>
<br>
<div class="card-body col-md-12 mb-12" dir="rtl" style="font-size: larger">
    <table border="1" class="table table-bordered" id="dataTable" width="100%" cellspacing="0" >
        <thead>
        <tr>
            <th>#</th>
            <th>رقم الامتحان </th>
            <th>أسم الامتحان </th>
            <th>نوع الامتحان </th>
            <th>الإجراء </th>

        </tr>
        </thead>

        <tbody>
        <?php


        $DBObject = new Database("localhost", "root", "", "exam_management_system_db");
        $DBObject->query ("SET NAMES utf8");
        $DBObject->query ("SET CHARACTER SET utf8");
        $sql = "Select * from exams ORDER BY id asc ;";

        $val = $DBObject->query($sql);

        While ($row = $val->fetch()) {
            if($row['exam_type']==1){
                $exam_type = "اختيار من متعدد";
            }elseif ($row['exam_type']==2){
                $exam_type = "صح أو خطاء";
            }


            echo '<tr>';
            echo '<td>' . $row['id'] . '</td>';
            echo '<td>' . $row['exam_id'] . '</td>';
            echo '<td>' . $row['exam_title'] . '</td>';
            echo '<td>' .$exam_type. '</td>';

            echo "<td> <a href=view.php?id=" . $row['exam_id'] . "><span class=\"glyphicon glyphicon-eye-open\"></span></a>
                          &nbsp;  <a href=update.php?id=" . $row['id'] . "><span class=\"fas fa-pencil-alt\"></span></a>
                            &nbsp;
                            <a href=add.php?id=" . $row['id'] . "><span class=\"fas fa-plus\"></span></a>
                           &nbsp; <a href=delete.php?id=" . $row['id']." onclick='return myFunction()'><span class=\"far fa-trash-alt\"></span></a>" . '</td>';
            echo '</tr>';
        }

        ?>

        </tbody>

    </table>
</div>


</body>
</html>

<script>
    function myFunction() {
        return confirm("Are You Sure!");
    }
</script>